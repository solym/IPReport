CXX=g++
LIBS= -lPocoNetSSL -lPocoNet -lPocoUtil -lPocoFoundation -lpthread

all:IPReport

IPReport:IPReport.cpp
	$(CXX) -std=c++11 -o $@ $^ $(LIBS)

clean:
	rm IPReport
