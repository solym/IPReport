#include <Poco/URI.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPSClientSession.h>
#include <Poco/Net/Context.h>
#include <Poco/Net/SSLException.h> 
#include <Poco/StreamCopier.h>
#include <Poco/Thread.h>
#include <Poco/Util/IniFileConfiguration.h>
#include <Poco/Net/MailMessage.h>
#include <Poco/Net/StringPartSource.h>
#include <Poco/Net/SMTPClientSession.h>
#include <Poco/Net/SecureSMTPClientSession.h>
#include <Poco/Exception.h>
#include <iostream>
#include <fstream>

using namespace Poco;
using namespace Poco::Util;
using namespace Poco::Net;

int main()
{
	static std::string mailuser,mailpasswd,mailhost,mailrecipient;
	AutoPtr<IniFileConfiguration> pConf(new IniFileConfiguration("test.ini"));
	mailuser      = pConf->getString("mailer.USER");
	mailpasswd    = pConf->getString("mailer.PASSWD");
	mailhost      = pConf->getString("mailer.HOST");
	mailrecipient = pConf->getString("mailer.RECIPIENT");

	static std::string lastResult("ipCallback({ip:\"222.111.24.174\"})");
	{
		// 不要一直占用
		std::fstream lastfile("./last.txt",std::ios::out | std::ios::in);
		if(!lastfile.is_open()){
			std::cerr<< "Open last.txt Failed!"<<std::endl;
			return 0;
		}
		lastfile >> lastResult;
		std::cout<<"last.txt内容:"<<lastResult<<std::endl;
	}

	while(true){
		Poco::Thread::sleep(7200); // 休眠2分钟
		try{
			Poco::URI uri("https://www.taobao.com/help/getip.php");
			Poco::Net::Context::Ptr context( 
						new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, 
							""/*PrivateKeyFile*/, ""/*CertificateFile*/, ""/*Calocation*/,
							Poco::Net::Context::VERIFY_NONE, 9, false),true);
			HTTPSClientSession s(uri.getHost(), uri.getPort(), context);
			HTTPRequest request(HTTPRequest::HTTP_GET,
						uri.getPathAndQuery(),Poco::Net::HTTPMessage::HTTP_1_1);
			// 发送请求
			s.sendRequest(request);
			// 获取响应
			HTTPResponse response;
			std::istream& rs = s.receiveResponse(response);
			// 读取返回结果
			std::string result;
			Poco::StreamCopier::copyToString(rs, result);
			if(lastResult == result){
				std::cout<<"没有变"<<std::endl;
				continue;
			}
			lastResult = result;
			std::cout<<lastResult<<std::endl;
			// 写入到文件中
			std::ofstream outlastfile("./last.txt",std::ios::out|std::ios::trunc);
			outlastfile << lastResult;
			// 发送邮件通知
			// 发送的消息内容
			Poco::Net::MailMessage message;
			message.setSubject("外网IP地址改变通知");
			message.setDate(Poco::DateTime().timestamp());
			message.addContent(new StringPartSource(lastResult,"text/plain"));
			message.setSender(mailuser);
			message.addRecipient(MailRecipient(MailRecipient::PRIMARY_RECIPIENT,mailrecipient));
			// 开始发送邮件
			// 第一个是非SSL连接的，第二个是SSL连接的
			//Poco::Net::SMTPClientSession smtpSession(mailhost);
			Poco::Net::SecureSMTPClientSession smtpSession(mailhost);
			smtpSession.open();
			// 下面两行是SSL连接必须的
			smtpSession.login();
			smtpSession.startTLS();
			// 登录邮件服务
			smtpSession.login(Poco::Net::SMTPClientSession::LoginMethod::AUTH_LOGIN, mailuser, mailpasswd);
			// 发送出邮件内容
			smtpSession.sendMessage(message);
			// 发送后关闭会话
			smtpSession.close();
		}
		catch (const Poco::Exception& e) {  
			std::cerr << "POCO Exception:"<< e.displayText() << std::endl;  
		}
		catch (const std::exception& e) {
			std::cerr << "STD Exception:" << e.what() << std::endl;;  
		}
	}
	return 0;
}
