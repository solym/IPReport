# IPReport

#### 项目介绍
外网IP变动自动上报工具。
写这个工具的目的是为了监测一台服务器的外网IP的变动情况。之前办理的电信宽带是有外网IP的，因此把一台小服务器上的ut管理端口开放出来，以便随时都能添加下载任务。
但是这个外网IP不是固定的，大概每周都会变动一次，变动的时间不固定，所以写了个程序来检测它，改变的时候主动发送信息给我。

#### 编译说明
程序依赖于[`Poco`](https://pocoproject.org/)库，需要自己准备。
因为我的服务器装的是`ArchLinux`，所以直接使用`pacman -Syu poco`安装就好了。
如果是`Windows`可以直接使用`vcpkg`来编译安装`poco`库。

**linux**下直接使用`make`编译即可。

#### 安装使用说明

无需安装，编译之后可以直接运行。
**linux**下可以使用`nohup ./IPReport 2>&1 1>/dev/null &`来放在后台运行。
**Windows**下你可以在VS工程`属性页->链接器->系统`里面选择子系统为`窗口(SUBSYSTEM:WINDOWS)`来生成一个无窗口的窗口应用，就可以无控制台运行了。

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)